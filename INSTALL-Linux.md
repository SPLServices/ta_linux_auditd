# Pre-implementation Procedure
* Review audit policies and ensure configuration will capture the events supporting the organization logging objectives without generating needless noisy events
* Complete data onboarding for Splunk TA Nix - linux

# Data Acquisition Procedure Linux Operating Systems

Data collection for security and IT operational use cases

## Server Role Splunk Enterprise (SH/IDX)
* Deploy to apps the following
	* `TA_linux-auditd`
	 
## Deployment Server Role "SRV"

* Stage the following apps to deployment-apps - If converting from Stock to best practices patterns fully remove the existing TAs, inputs and server classes. Files not present in the repo might not be replaced causing unexpected results
	* `TA_linux-auditd`
	* `TA_linux-auditd_seckit_0_inputs`

* Install the following apps on the deployment server
	* `SecKit_all_deploymentserver_2_oslinuxauditd`
* Update `SecKit_all_deploymentserver_2_oslinuxauditd/local/serverclass.conf` define the whitelist.0 to capture all hosts where auditd is configured for logging. In most cases this should apply to all servers. 

``` text
[seckit_all_2_os_linuxauditd_0]
whitelist.0= ^- 
```
